#pragma once
#include "utility.h"

#define  TEXTURE_SIZE 128//“一坨水”是因为像素太低，网上的效果那么好是因为直接在原图片上处理像素——》并不是这个原因，sample时利用scale+wrap提高resolution，还是这卵样……
#define  PUSH_SCALE 10
#define PUSH_RADIUS 8
#define ANIMATIONS_PER_SECOND 35.0f

class WaterRipple2DDemo:public MyBaseApplication
{
private:
	HardwarePixelBufferSharedPtr mTexBuf;
	float mHeightBuf[2][TEXTURE_SIZE][TEXTURE_SIZE];//其实可以用一个float**数组存放两个二维数组的索引
	int mCurrentBuf;
	uint8 mIntermediateTexBuf[TEXTURE_SIZE*TEXTURE_SIZE*2];//for memcpy

	Real lastFrameTime ;//just for push
	Real animationTime;//for recurrence
	Real timeFragment;//来自上一帧不足以完成一次状态池更换的残留时间 

	const float damping;

private:
	void push(int x,int y,float depth,bool absolute=false)//不像只是push一个点这么基本，这里直接封装push范围为圆形
	{
		// scale pressure according to time passed
		depth = depth * lastFrameTime * ANIMATIONS_PER_SECOND ;

		for(int i=-PUSH_RADIUS;i++;i<=PUSH_RADIUS)
		{
			for(int j=-PUSH_RADIUS;j++;j<=PUSH_RADIUS)
			{
				if(i+x>0 && i+x<TEXTURE_SIZE-1 && j+y>0 && j+y<TEXTURE_SIZE-1)
				{
					int dist=Math::Sqrt(i*i+j*j);
					if(dist<PUSH_RADIUS)
					{
						if(absolute)
							mHeightBuf[mCurrentBuf][x+i][y+j]=Math::Cos(1.57*dist/PUSH_RADIUS)*depth;
						else
							mHeightBuf[mCurrentBuf][x+i][y+j]+=Math::Cos(1.57*dist/PUSH_RADIUS)*depth;
					}
				}

			}
		}


	}

	void UpdateBuf(Real timeSinceLastFrame)
	{
		lastFrameTime=timeSinceLastFrame;
		timeFragment+=timeSinceLastFrame;

		while(timeFragment>=animationTime)
		{
			int nextBuf=(mCurrentBuf+1)%2;

			for(int i=1;i<TEXTURE_SIZE-1;i++)
			{
				for(int j=1;j<TEXTURE_SIZE-1;j++)
				{

					/*if(i==64&&j==64)
					{
					int caonimai=2;
					}*/
					

				/*	mHeightBuf[nextBuf][i][j]=(mHeightBuf[mCurrentBuf][i-1][j-1]+
															mHeightBuf[mCurrentBuf][i-1][j]+
															mHeightBuf[mCurrentBuf][i-1][j+1]+
															mHeightBuf[mCurrentBuf][i][j-1]+
															mHeightBuf[mCurrentBuf][i][j+1]+
															mHeightBuf[mCurrentBuf][i+1][j-1]+
															mHeightBuf[mCurrentBuf][i+1][j]+
															mHeightBuf[mCurrentBuf][i+1][j+1])/4.0f-mHeightBuf[nextBuf][i][j];*/
					//如果用-mHeightBuf[mCurrentBuf][i][j];设断点监视mHeightBuf[mCurrentBuf][64][64]发现不是收敛的而是向正负无穷大两边振荡……

					mHeightBuf[nextBuf][i][j]=(mHeightBuf[mCurrentBuf][i-1][j]+
						mHeightBuf[mCurrentBuf][i][j-1]+
						mHeightBuf[mCurrentBuf][i][j+1]+
						mHeightBuf[mCurrentBuf][i+1][j])/2.0f-mHeightBuf[nextBuf][i][j];

					mHeightBuf[nextBuf][i][j]*=damping;
				}
			}

			mCurrentBuf=nextBuf;
			timeFragment-=animationTime;
		}

		WriteToTexture();
	}

	void WriteToTexture()//也可以说是calculate fake normal吧
	{
		for(int i=1;i<TEXTURE_SIZE-1;i++)
		{
			for(int j=1;j<TEXTURE_SIZE-1;j++)
			{
				float xdh=(mHeightBuf[mCurrentBuf][i][j]-mHeightBuf[mCurrentBuf][i][j+1])-
					(mHeightBuf[mCurrentBuf][i][j]-mHeightBuf[mCurrentBuf][i][j-1]);
				float ydh=(mHeightBuf[mCurrentBuf][i][j]-mHeightBuf[mCurrentBuf][i+1][j])-
					(mHeightBuf[mCurrentBuf][i][j]-mHeightBuf[mCurrentBuf][i-1][j]);
				int index=2*(i*TEXTURE_SIZE+j);
				mIntermediateTexBuf[index]=(xdh*PUSH_SCALE+1.0f)/2.0f*255;
				mIntermediateTexBuf[index+1]=(ydh*PUSH_SCALE+1.0f)/2.0f*255;
			}
		}

		memcpy(mTexBuf->lock(HardwareBuffer::HBL_DISCARD), mIntermediateTexBuf, 2*TEXTURE_SIZE * TEXTURE_SIZE);
		mTexBuf->unlock();
	}



public:
	WaterRipple2DDemo():mCurrentBuf(0),mTexBuf(nullptr),damping(1.0f-1.0f/16.0f),
		animationTime(1.0f/ANIMATIONS_PER_SECOND),lastFrameTime(0.0f),timeFragment(0.0f)
	{
		for(int i=0;i<TEXTURE_SIZE;i++)
		{
			for(int j=0;j<TEXTURE_SIZE;j++)
			{
				mHeightBuf[0][i][j]=0.0f;
				mHeightBuf[1][i][j]=0.0f;
			}
		}

		memset(mIntermediateTexBuf,0,sizeof(mIntermediateTexBuf));		
	}


	bool frameStarted(const FrameEvent& evt)
	{
		if(MyBaseApplication::frameStarted(evt)==false)
			return false;

		static bool pressedBefore=false;
		if(_key->isKeyDown(OIS::KC_SPACE))
		{
			if(!pressedBefore)
			{
				pressedBefore=true;
				push(TEXTURE_SIZE/2,TEXTURE_SIZE/2,-10);
			}
		}
		else
		{
			pressedBefore=false;
		}

		UpdateBuf(evt.timeSinceLastFrame);

		return true;
	}


protected:
	void CreateScene()
	{

		// create our dynamic texture with 8-bit luminance texels
		TexturePtr tex = TextureManager::getSingleton().createManual("ripple", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
			TEX_TYPE_2D, TEXTURE_SIZE, TEXTURE_SIZE, 0, PF_BYTE_LA, TU_DYNAMIC_WRITE_ONLY);
		mTexBuf = tex->getBuffer();  // save off the texture buffer
		memcpy(mTexBuf->lock(HardwareBuffer::HBL_DISCARD), mIntermediateTexBuf, 2*TEXTURE_SIZE * TEXTURE_SIZE);
		mTexBuf->unlock();
		

		Ogre::CompositorManager::getSingleton().addCompositor(_vp,"My2DWaterRipple");
		Ogre::CompositorManager::getSingleton().setCompositorEnabled(_vp,"My2DWaterRipple",true);
	}
	
};