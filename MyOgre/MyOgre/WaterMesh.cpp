#include "WaterMesh.h"

#define ANIMATIONS_PER_SECOND 100.0f

WaterMesh::WaterMesh(const String& inMeshName, Real planeSize, int inComplexity)
{
	meshName = inMeshName ;
	complexity = inComplexity ;
	numFaces = 2 * complexity * complexity;
	numVertices = (complexity + 1) * (complexity + 1) ;

	lastFrameTime = 0 ;
	timeFragment=0;
	animationTime=1.0f/ANIMATIONS_PER_SECOND;

	// initialize algorithm parameters
	C = 0.3f ; // ripple speed
	D = 0.4f ; // distance
	U = 0.05f ; // viscosity
	T = 0.13f ; // time

	// allocate space for normal calculation
	vNormals = new Vector3[numVertices];

	// create mesh and submesh
	mesh = MeshManager::getSingleton().createManual(meshName,
        ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	subMesh = mesh->createSubMesh();
	subMesh->useSharedVertices=false;

	// Vertex buffers
	subMesh->vertexData = new VertexData();
	subMesh->vertexData->vertexStart = 0;
	subMesh->vertexData->vertexCount = numVertices;

	VertexDeclaration* vdecl = subMesh->vertexData->vertexDeclaration;
	VertexBufferBinding* vbind = subMesh->vertexData->vertexBufferBinding;
	vdecl->addElement(0, 0, VET_FLOAT3, VES_POSITION);
	vdecl->addElement(1, 0, VET_FLOAT3, VES_NORMAL);
	vdecl->addElement(2, 0, VET_FLOAT2, VES_TEXTURE_COORDINATES);

	// Prepare buffer for positions
	posVertexBuffer =
         HardwareBufferManager::getSingleton().createVertexBuffer(
            3*sizeof(float),
			numVertices,
			HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
	vbind->setBinding(0, posVertexBuffer);

	// Prepare buffer for normals - write only
	normVertexBuffer =
         HardwareBufferManager::getSingleton().createVertexBuffer(
            3*sizeof(float),
			numVertices,
			HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
	vbind->setBinding(1, normVertexBuffer);

	// Prepare texture coords buffer - static one
	// todo: optimize to write directly into buffer
	float *texcoordsBufData = new float[numVertices*2];
	for(int y=0;y<=complexity;y++) {
		for(int x=0;x<=complexity;x++) {
			texcoordsBufData[2*(y*(complexity+1)+x)+0] = (float)x / complexity ;
			texcoordsBufData[2*(y*(complexity+1)+x)+1] = 1.0f - ((float)y / (complexity)) ;
		}
	}
	texcoordsVertexBuffer =
         HardwareBufferManager::getSingleton().createVertexBuffer(
            2*sizeof(float),
			numVertices,
			HardwareBuffer::HBU_STATIC_WRITE_ONLY);
	texcoordsVertexBuffer->writeData(0,
		texcoordsVertexBuffer->getSizeInBytes(),
		texcoordsBufData,
		true); // true?
	delete [] texcoordsBufData;
    vbind->setBinding(2, texcoordsVertexBuffer);

	// Prepare buffer for indices
	indexBuffer =
		HardwareBufferManager::getSingleton().createIndexBuffer(
			HardwareIndexBuffer::IT_16BIT,
			3*numFaces,
			HardwareBuffer::HBU_STATIC, true);
	unsigned short *faceVertexIndices = (unsigned short*)
		indexBuffer->lock(0, numFaces*3*2, HardwareBuffer::HBL_DISCARD);
	for(int y=0 ; y<complexity ; y++) {
		for(int x=0 ; x<complexity ; x++) {
			unsigned short *twoface = faceVertexIndices + (y*complexity+x)*2*3;
			int p0 = y*(complexity+1) + x ;
			int p1 = y*(complexity+1) + x + 1 ;
			int p2 = (y+1)*(complexity+1) + x ;
			int p3 = (y+1)*(complexity+1) + x + 1 ;
			twoface[0]=p2; //first tri
			twoface[1]=p1;
			twoface[2]=p0;
			twoface[3]=p2; //second tri
			twoface[4]=p3;
			twoface[5]=p1;
		}
	}
	indexBuffer->unlock();
	// Set index buffer for this submesh
	subMesh->indexData->indexBuffer = indexBuffer;
	subMesh->indexData->indexStart = 0;
	subMesh->indexData->indexCount = 3*numFaces;

	/*	prepare vertex positions
	 *	note - we use 3 position buffers, since algorighm uses two last phases
	 *	to calculate the next one
	 */
	for(int b=0;b<3;b++) {
		posBuffers[b] = new Vector3[numVertices] ;
		for(int y=0;y<=complexity;y++) {
			for(int x=0;x<=complexity;x++) {
				int numPoint = y*(complexity+1) + x ;
				posBuffers[b][numPoint].x=(float)(x) / (float)(complexity) * (float) planeSize ;
				posBuffers[b][numPoint].y= 0 ; 
				posBuffers[b][numPoint].z=(float)(y) / (float)(complexity) * (float) planeSize ;
			}
		}
	}

	AxisAlignedBox meshBounds(0,0,0,
		planeSize,0, planeSize);
	mesh->_setBounds(meshBounds);

	currentBuffNumber = 0 ;
	posVertexBuffer->writeData(0,
		posVertexBuffer->getSizeInBytes(), // size
		posBuffers[currentBuffNumber], // source,Vector3型的应该行不通？行不通就只能lock+memcpy了
		true); // discard?
	
    mesh->load();
    mesh->touch();

}


WaterMesh::~WaterMesh ()
{
	delete[] posBuffers[0];
	delete[] posBuffers[1];
	delete[] posBuffers[2];

	delete[] vNormals;

	MeshManager::getSingleton().remove(meshName);
}

void WaterMesh::push(Real x, Real y, Real depth, bool absolute)
{
	//float *buf = (float*)(posBuffers[currentBuffNumber])+1 ;//just for y coordinate
	// scale pressure according to time passed
	depth = depth * lastFrameTime * ANIMATIONS_PER_SECOND ;
#define _PREP(addx,addy) { \
	int vertexIndex=(int)(y+addy)*(complexity+1)+(int)(x+addx); \
	float diffy = y - floor(y+addy); \
	float diffx = x - floor(x+addx); \
	float dist=sqrt(diffy*diffy + diffx*diffx) ; \
	float power = 1 - dist ; \
	if (power<0)  \
	power = 0; \
	if (absolute) \
	posBuffers[currentBuffNumber][vertexIndex].y = depth*power ;  \
	else \
	posBuffers[currentBuffNumber][vertexIndex].y += depth*power ;  \
	} /* #define */
	_PREP(0,0);
	_PREP(0,1);
	_PREP(1,0);
	_PREP(1,1);
#undef _PREP
}


Real WaterMesh::getHeight(Real x, Real y)
{
	int x0 = int(x);
	int y0 = int(y);
	int p0=y0*(complexity+1)+x0;
	int p1=p0+1;
	int p2=p0+complexity+1;
	int p3=p2+1;
	float h0=posBuffers[currentBuffNumber][p0].y;
	float h1=posBuffers[currentBuffNumber][p1].y;
	float h2=posBuffers[currentBuffNumber][p2].y;
	float h3=posBuffers[currentBuffNumber][p3].y;
	//lerp
	float hy1=h0+(x-(Real)x0)*(h1-h0);
	float hy2=h2+(x-(Real)x0)*(h3-h2);
	return hy1+(y-(Real)y0)*(hy2-hy1);

}


void WaterMesh::calculateNormals()
{
	// zero normals
	for(int i=0;i<numVertices;i++) {
		vNormals[i] = Vector3::ZERO;
	}
	// first, calculate normals for faces, add them to proper vertices
	unsigned short* vinds = (unsigned short*) indexBuffer->lock(
		0, indexBuffer->getSizeInBytes(), HardwareBuffer::HBL_READ_ONLY);
	float *pNormals = (float*) normVertexBuffer->lock(
		0, normVertexBuffer->getSizeInBytes(), HardwareBuffer::HBL_DISCARD);
	for(int i=0;i<numFaces;i++) {
		int p0 = vinds[3*i] ;
		int p1 = vinds[3*i+1] ;
		int p2 = vinds[3*i+2] ;
		Vector3 diff1 = posBuffers[currentBuffNumber][p2] - posBuffers[currentBuffNumber][p1] ;
		Vector3 diff2 = posBuffers[currentBuffNumber][p0] - posBuffers[currentBuffNumber][p1] ;
		Vector3 fn = diff1.crossProduct(diff2);
		vNormals[p0] += fn ;
		vNormals[p1] += fn ;
		vNormals[p2] += fn ;
	}
	// now normalize vertex normals
	for(int y=0;y<=complexity;y++) {
		for(int x=0;x<=complexity;x++) {
			int numPoint = y*(complexity+1) + x ;
			Vector3 n = vNormals[numPoint] ;
			n.normalise() ;
			float* normal = pNormals + 3*numPoint ;
			normal[0]=n.x;
			normal[1]=n.y;
			normal[2]=n.z;
		}
	}
	indexBuffer->unlock();
	normVertexBuffer->unlock();
}

void WaterMesh::updateMesh(Real timeSinceLastFrame)
{
	lastFrameTime = timeSinceLastFrame ;
	timeFragment += timeSinceLastFrame ;

	// do rendering to get ANIMATIONS_PER_SECOND
	while(timeFragment >=animationTime ) 
	{
		// switch buffer numbers
		currentBuffNumber = (currentBuffNumber + 1) % 3 ;
		Vector3* buf = posBuffers[currentBuffNumber] ;
		Vector3* buf1 = posBuffers[(currentBuffNumber+2)%3] ;
		Vector3* buf2 = posBuffers[(currentBuffNumber+1)%3] ;

		/* we use an algorithm from
		 * http://collective.valve-erc.com/index.php?go=water_simulation
		 * The params could be dynamically changed every frame of course
		 */
		Real TERM1 = ( 4.0f - 8.0f*C*C*T*T/(D*D) ) / (U*T+2) ;
		Real TERM2 = ( U*T-2.0f ) / (U*T+2.0f) ;
		Real TERM3 = ( 2.0f * C*C*T*T/(D*D) ) / (U*T+2) ;
		for(int y=1;y<complexity;y++)
		{ // don't do anything with border values
			Vector3* row = buf + y*(complexity+1) ;
			Vector3* row1 = buf1 + y*(complexity+1) ;
			Vector3* row1up = buf1 + (y-1)*(complexity+1) ;
			Vector3* row1down = buf1 + (y+1)*(complexity+1) ;
			Vector3* row2 = buf2 + y*(complexity+1) ;
			for(int x=1;x<complexity;x++) 
			{
				row[x].y = TERM1 * row1[x].y
					+ TERM2 * row2[x].y
					+ TERM3 * ( row1[x-1].y + row1[x+1].y + row1up[x].y+row1down[x].y ) ;
			}
		}

		timeFragment-=animationTime;
	}

	calculateNormals();

	// set vertex buffer
	posVertexBuffer->writeData(0,
		posVertexBuffer->getSizeInBytes(), // size
		posBuffers[currentBuffNumber], // source
		true); // discard?
}