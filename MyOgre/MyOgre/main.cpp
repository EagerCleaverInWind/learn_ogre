//#include "FresnelDemo.h"
//#include "PSNodeAnimDemo.h"
//#include "OceanDemo.h"
#include "Water.h"
//#include "WaterRipple2D.h"

//#pragma comment(lib, "OgreMain_d.lib")  
//#pragma comment(lib, "OIS_d.lib")  
//#pragma comment(lib, "OgreOverlay_d.lib")  



int main(int argc, int *agrv[])  
{  
	MyWater demo;
	demo.Startup();

	while(demo.KeepRunning())
	{
		demo.RenderOneFrame();
	}

	return 0; 
}  