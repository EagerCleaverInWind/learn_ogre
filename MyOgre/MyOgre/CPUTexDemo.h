#pragma  once

#include "utility.h"

class CPUTexDemo:public MyBaseApplication
{
private:
	const unsigned int TEXTURE_SIZE;
	HardwarePixelBufferSharedPtr mTexBuf;
	//uint8* mConstantTexBuf;//精度太低，无法用来累积（每次在加到过整之前就已经被舍去）
	float* mConstantTexBuf;
	uint8* mIntermediateTexBuf;//for memcpy
	Real mPlaneSize;
	Real mTimeSinceLastFreeze;

	SceneNode* mPenguinNode;
	AnimationState* mPenguinAnimState;

private:
	void updateTexture(uint8 freezeAmount);
	void UpdateTexture(uint8 freezeAmount);

protected:
	void CreateScene();

public:
	CPUTexDemo(): TEXTURE_SIZE(128)
	{

	}

	~CPUTexDemo()
	{
		delete [] mConstantTexBuf;
		delete [] mIntermediateTexBuf;
		TextureManager::getSingleton().remove("thaw");
	}

	bool frameStarted(const FrameEvent& evt);
};