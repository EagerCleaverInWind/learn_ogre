#pragma  once
#include "utility.h"

class CompositorListener : public Ogre::CompositorInstance::Listener
{
private:
	MaterialPtr _mat;
	int _pass_id;

	float number;

public:
	CompositorListener()
	{
		number = 125.0f;
		_pass_id = 0;
	}

	void notifyMaterialSetup(uint32 pass_id, MaterialPtr &mat)
	{
		_mat = mat;
		_pass_id = pass_id;
	}

	void setNumber(float num)
	{
		number = num;
		_mat->getBestTechnique()->getPass(_pass_id)->getFragmentProgramParameters()->setNamedConstant("numpixels",number);
	}

	float getNumber()
	{
		return number;
	}
};


class MyFrameListener:public Ogre::FrameListener
{
private:
	//Model
	Ogre::SceneNode* _node;
	Ogre::Entity* _ent;
	Ogre::AnimationState* _runBase;
	Ogre::AnimationState* _runTop;
	float _WalkingSpeed;
	float _rotation;

	//Camera
	Ogre::Camera* _Cam;
	Ogre::PolygonMode _PolyMode;
	bool _pressedBefore;
	float _movementspeed;

	//Input System
	OIS::InputManager* _man;
	OIS::Keyboard* _key;
	OIS::Mouse* _mouse;

	CompositorListener* _comListener;


private:
	void UpdateCamera(float dt);
	void UpdateModel(float dt);
	void UpdateCompositor();

public:
	MyFrameListener(Ogre::SceneNode* node,Ogre::Entity* ent,RenderWindow* win,Ogre::Camera* cam,CompositorListener* comListener)
	{
		_node = node;
		_WalkingSpeed = 50.0f;
		_rotation = 0.0f;
		_ent=ent;
		_runBase = _ent->getAnimationState("RunBase");
		_runBase->setLoop(false);
		_runTop = _ent->getAnimationState("RunTop");
		_runTop->setLoop(false);

		_Cam = cam;
		_pressedBefore = false;
		_movementspeed = 200.0f;
		_PolyMode = Ogre::PolygonMode::PM_SOLID;

		size_t windowHnd = 0;
		std::stringstream windowHndStr;
		win->getCustomAttribute("WINDOW", &windowHnd);
		windowHndStr << windowHnd;
		OIS::ParamList pl;
		pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));
		_man = OIS::InputManager::createInputSystem( pl );
		_key = static_cast<OIS::Keyboard*>(_man->createInputObject( OIS::OISKeyboard, false ));
		_mouse = static_cast<OIS::Mouse*>(_man->createInputObject( OIS::OISMouse, false ));

		_comListener=comListener;
	}

	~MyFrameListener()
	{
		_man->destroyInputObject(_key);
		_man->destroyInputObject(_mouse);
		OIS::InputManager::destroyInputSystem(_man);
	}

	bool frameStarted(const FrameEvent& evt);

};


class MyApplication
{  
private:
	Ogre::Root* _root;
	Ogre::SceneManager* _sceneManager;
	bool _keepRunning;

	MyFrameListener* _frameListener;
	CompositorListener* _comListener;

	Ogre::SceneNode* _SinbadNode;
	Ogre::Entity* _SinbadEnt;

	Ogre::Viewport* vp1;
	Ogre::Viewport* vp2;


private:
	void loadResources();
	void createScene();

public:  
	MyApplication()
	{
		_comListener=nullptr;
		_sceneManager = nullptr;
		_frameListener = nullptr;
		_keepRunning = true;
	}

	~MyApplication()
	{
		SAFE_DELETE(_frameListener);
		SAFE_DELETE(_comListener);
		SAFE_DELETE(_root);//����ͷ�
	}

	int Startup();
	void RenderOneFrame();
	bool KeepRunning(){return _keepRunning;}

};  