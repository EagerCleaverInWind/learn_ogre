#pragma  once

#include "Ogre\Ogre.h"
#include "OIS\OIS.h"
using namespace Ogre;

#define  SAFE_DELETE(p) {if(p){delete (p);(p)=nullptr;}}


class MyBaseApplication:public FrameListener
{
protected:
	Ogre::Root* _root;
	Ogre::RenderWindow* _window;
	Ogre::SceneManager* _sceneManager;
	bool _keepRunning;

	//Camera
	Ogre::Camera* _cam;
	Ogre::PolygonMode _polyMode;
	bool _pressedBefore;
	float _movementspeed;
	Ogre::Viewport* _vp;

	//Input System
	OIS::InputManager* _inputManager;
	OIS::Keyboard* _key;
	OIS::Mouse* _mouse;

private://没法protected virtual，因为它们已经被virtual成员函数调用
	void LoadResources();
	void InitInput();
	void UpdateCamera(float dt);

protected:
	virtual void CreateScene()=0;


public:
	MyBaseApplication()
	{
		_root=nullptr;
		_sceneManager=nullptr;
		_keepRunning=true;

		_cam = nullptr;
		_pressedBefore = false;
		_movementspeed = 200.0f;
		_polyMode = Ogre::PolygonMode::PM_SOLID;

		_inputManager=nullptr;
		_key=nullptr;
		_mouse=nullptr;
	}

	~MyBaseApplication()
	{
		_inputManager->destroyInputObject(_key);
		_inputManager->destroyInputObject(_mouse);
		OIS::InputManager::destroyInputSystem(_inputManager);

		SAFE_DELETE(_root);//最后释放
	}
	
	virtual bool frameStarted(const FrameEvent& evt);//哈哈，这个要重载，就不能首字母大写了

	virtual int Startup();
	void RenderOneFrame();
	bool KeepRunning(){return _keepRunning;}

};