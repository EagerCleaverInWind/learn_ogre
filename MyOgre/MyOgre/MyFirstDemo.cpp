#include "MyFirstDemo.h"

void MyFrameListener::UpdateCamera(float dt)
{
	Ogre::Vector3 translate(0,0,0);

	if(_key->isKeyDown(OIS::KC_W))
	{
		translate += Ogre::Vector3(0,0,-1);
	}
	if(_key->isKeyDown(OIS::KC_S))
	{
		translate += Ogre::Vector3(0,0,1);
	}
	if(_key->isKeyDown(OIS::KC_A))
	{
		translate += Ogre::Vector3(-1,0,0);
	}
	if(_key->isKeyDown(OIS::KC_D))
	{
		translate += Ogre::Vector3(1,0,0);
	}
	translate.normalise();

	if(_key->isKeyDown(OIS::KC_R) && !_pressedBefore)
	{
		_pressedBefore = true;
		if(_PolyMode == PM_SOLID) 
		{
			_PolyMode = Ogre::PolygonMode::PM_WIREFRAME;
		} 
		else if(_PolyMode == PM_WIREFRAME) 
		{
			_PolyMode = Ogre::PolygonMode::PM_POINTS;
		} 
		else if(_PolyMode == PM_POINTS) 
		{
			_PolyMode = Ogre::PolygonMode::PM_SOLID;
		} 
		_Cam->setPolygonMode(_PolyMode);
	}
	if(!_key->isKeyDown(OIS::KC_R))
	{
		_pressedBefore = false;
	}

	float rotX = _mouse->getMouseState().X.rel * dt* -1;
	float rotY = _mouse->getMouseState().Y.rel * dt * -1;


	_Cam->yaw(Ogre::Radian(rotX));
	_Cam->pitch(Ogre::Radian(rotY));
	_Cam->moveRelative(translate*dt * _movementspeed);
}

void MyFrameListener::UpdateModel(float dt)
{
	Ogre::Vector3 translate(0,0,0);
	bool walked=false;
	if(_key->isKeyDown(OIS::KC_UP))
	{
		translate += Ogre::Vector3(0,0,-1);
		_rotation = 3.14f;
		walked = true;
	}
	if(_key->isKeyDown(OIS::KC_DOWN))
	{
		translate += Ogre::Vector3(0,0,1);
		_rotation = 0.0f;
		walked = true;
	}
	if(_key->isKeyDown(OIS::KC_LEFT))
	{
		translate += Ogre::Vector3(-1,0,0);
		_rotation = -1.57f;
		walked = true;
	}
	if(_key->isKeyDown(OIS::KC_RIGHT))
	{
		translate += Ogre::Vector3(1,0,0);
		_rotation = 1.57f;
		walked = true;
	}
	if(translate.x*translate.z!=0)
	{
		switch (int(translate.x))
		{
		case 1:
			switch (int(translate.z))
			{
			case 1:
				_rotation=3.14/4.0f;
				break;
			case -1:
				_rotation=3.14-3.14/4.0f;
				break;
			default:
				break;
			}
			break;

		case -1:
			switch (int(translate.z))
			{
			case 1:
				_rotation=-3.14/4.0f;
				break;
			case -1:
				_rotation=3.14+3.14/4.0f;
				break;
			default:
				break;
			}
			break;

		default:
			break;
		}
	}
	translate.normalise();

	if(walked)
	{
		_runBase->setEnabled(true);
		_runTop->setEnabled(true);
		_runBase->addTime(dt);
		_runTop->addTime(dt);
		if(_runBase->hasEnded())
		{	
			_runBase->setTimePosition(0.0f);
		}
		if(_runTop->hasEnded())
		{
			_runTop->setTimePosition(0.0f);
		}
	}
	else
	{
		_runBase->setTimePosition(0.0f);
		_runBase->setEnabled(false);
		_runTop->setTimePosition(0.0f);
		_runTop->setEnabled(false);
	}

	_node->translate(translate * dt * _WalkingSpeed);
	_node->resetOrientation();
	_node->yaw(Ogre::Radian(_rotation));
}

void MyFrameListener::UpdateCompositor()
{
	if(_key->isKeyDown(OIS::KC_Q))
	{
		float num = _comListener->getNumber();
		if(num<1024)
			num++;
		_comListener->setNumber(num);
	}

	if(_key->isKeyDown(OIS::KC_E))
	{
		float num = _comListener->getNumber();
		if(num>1)
			num--;
		_comListener->setNumber(num);
	}
}

bool MyFrameListener::frameStarted(const FrameEvent& evt)
{
	_key->capture();
	_mouse->capture();

	if(_key->isKeyDown(OIS::KC_ESCAPE))
	{
		return false;
	}

	float dt=evt.timeSinceLastFrame;
	UpdateCamera(dt);
	UpdateModel(dt);
	UpdateCompositor();

	return true;
}



void MyApplication::loadResources()
{
	Ogre::ConfigFile cf;
	cf.load("resources_d.cfg");

	Ogre::ConfigFile::SectionIterator sectionIter = cf.getSectionIterator();
	Ogre::String sectionName, typeName, dataname;
	while (sectionIter.hasMoreElements())
	{
		sectionName = sectionIter.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = sectionIter.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for (i = settings->begin(); i != settings->end(); ++i)
		{
			typeName = i->first;
			dataname = i->second;

			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
				dataname, typeName, sectionName);

		}
	}

	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

void MyApplication::createScene() 
{  
	_sceneManager->setAmbientLight(Ogre::ColourValue(0.15f, 0.15f, 0.15f));

	Ogre::Plane plane(Vector3::UNIT_Y, -10);
	Ogre::MeshManager::getSingleton().createPlane("plane",
		ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane,
		1500,1500,200,200,true,1,25,25,Vector3::UNIT_Z);

	Ogre::Entity* ent = _sceneManager->createEntity("LightPlaneEntity", "plane");
	_sceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(ent);
	ent->setMaterialName("Examples/BeachStones");
	ent->setCastShadows(false);

	Ogre::SceneNode* node = _sceneManager->createSceneNode("node");
	_sceneManager->getRootSceneNode()->addChild(node);

	Ogre::Light* light = _sceneManager->createLight("light");
	light->setType(Ogre::Light::LT_DIRECTIONAL);
	light->setDirection(Ogre::Vector3(1,-1,0));

	_SinbadEnt= _sceneManager->createEntity("Sinbad", "Sinbad.mesh");
	_SinbadNode = node->createChildSceneNode("SinbadNode");
	_SinbadNode->setScale(3.0f,3.0f,3.0f);
	_SinbadNode->setPosition(Ogre::Vector3(0.0f,4.0f,0.0f));
	_SinbadNode->attachObject(_SinbadEnt);


	_sceneManager->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE);


	Ogre::ParticleSystem* partSystem = _sceneManager->createParticleSystem("hasaki","MyCyclone");
	_SinbadNode->attachObject(partSystem);
}  

int MyApplication::Startup()
{
	_root = new Ogre::Root("plugins_d.cfg");
	if(!_root->showConfigDialog())
	{
		return -1;
	}

	Ogre::RenderWindow* window = _root->initialise(true,"MyOgre");

	_sceneManager = _root->createSceneManager(Ogre::ST_GENERIC);

	Ogre::Camera* camera = _sceneManager->createCamera("camera1");
	camera->setPosition(Ogre::Vector3(0,100,200));
	camera->lookAt(Ogre::Vector3(0,0,0));
	camera->setNearClipDistance(5);

	vp1 = window->addViewport(camera,0,0.0,0.0,0.5,1.0);
	vp1->setBackgroundColour(ColourValue(0.0f,0.0f,0.0f));
	vp2 = window->addViewport(camera,1,0.5,0.0,0.5,1.0);
	vp2->setBackgroundColour(ColourValue(0.0f,0.0f,0.0f));
	camera->setAspectRatio(Real(vp1->getActualWidth()) / Real(vp1->getActualHeight()));

	loadResources();
	createScene();

	//监听器一定要放在createScene()后面
	Ogre::CompositorManager::getSingleton().addCompositor(vp2, "CompositorPixelfy");
	Ogre::CompositorManager::getSingleton().setCompositorEnabled(vp2, "CompositorPixelfy", true);
	Ogre::CompositorInstance* comp =  Ogre::CompositorManager::getSingleton().getCompositorChain(vp2)->getCompositor("CompositorPixelfy");
	_comListener = new CompositorListener();
	comp->addListener(_comListener);

	_frameListener = new MyFrameListener(_SinbadNode,_SinbadEnt,window,camera,_comListener);
	_root->addFrameListener(_frameListener);


	return 0;
}

void MyApplication::RenderOneFrame()
{
	Ogre::WindowEventUtilities::messagePump();
	_keepRunning = _root->renderOneFrame();
}