#pragma  once
#include "WaterMesh.h"
#include "OgreBillboardParticleRenderer.h"

#define MESH_NAME "WaterMesh"
#define MATERIAL_NAME "Examples/Water8"
#define COMPLEXITY 128		// 调到256CPU就开始不行了，512完全崩了……毕竟(complexity+1)^2
#define PLANE_SIZE 3000.0f

#define RAIN_HEIGHT_RANDOM 7
#define RAIN_HEIGHT_CONSTANT 50//28

class MyWater:public MyBaseApplication
{
private:
	WaterMesh* mpWaterMesh;
	ParticleSystem *particleSystem ;
	ParticleEmitter *particleEmitter ;


private:
	void processParticles()
	{
		static int pindex = 0 ;
		ParticleIterator pit = particleSystem->_getIterator() ;
		while(!pit.end()) {
			Particle *particle = pit.getNext();
			Vector3 ppos = particle->position;
			if (ppos.y<=0 && particle->timeToLive>0) { // hits the water!
				// delete particle
				particle->timeToLive = 0.0f;
				// push the water
				float x = ppos.x / PLANE_SIZE * COMPLEXITY ;
				float y = ppos.z / PLANE_SIZE * COMPLEXITY ;
				float h = rand() % RAIN_HEIGHT_RANDOM + RAIN_HEIGHT_CONSTANT ;
				if (x<1) x=1 ;
				if (x>COMPLEXITY-1) x=COMPLEXITY-1;
				if (y<1) y=1 ;
				if (y>COMPLEXITY-1) y=COMPLEXITY-1;
				mpWaterMesh->push(x,y,-h) ;
			}
		}
	}


protected:
	void CreateScene()
	{
		// Set ambient light
		_sceneManager->setAmbientLight(ColourValue(0.75, 0.75, 0.75));
		// Create a light
		Light* pointLight = _sceneManager->createLight("MainLight");
		pointLight->setPosition(200,300,100);

		_cam->setPosition(0, 550, PLANE_SIZE);
		_cam->lookAt(0,0,0);

		// Create water mesh and entity
		mpWaterMesh = new WaterMesh(MESH_NAME, PLANE_SIZE, COMPLEXITY);
		Entity* waterEntity = _sceneManager->createEntity("WaterEntity",MESH_NAME);
		waterEntity->setMaterialName(MATERIAL_NAME);
		SceneNode *waterNode = _sceneManager->getRootSceneNode()->createChildSceneNode();
		waterNode->attachObject(waterEntity);

		// Let there be rain
		particleSystem = _sceneManager->createParticleSystem("rain",
			"Examples/Water/Rain");
		particleEmitter = particleSystem->getEmitter(0);
		particleEmitter->setEmissionRate(0);
		SceneNode* rNode = _sceneManager->getRootSceneNode()->createChildSceneNode();
		rNode->translate(PLANE_SIZE/2.0f, 3000, PLANE_SIZE/2.0f);
		rNode->attachObject(particleSystem);
		// Fast-forward the rain so it looks more natural
		particleSystem->fastForward(20);
		static_cast<BillboardParticleRenderer*>( particleSystem->getRenderer() )->setBillboardOrigin(BBO_BOTTOM_CENTER);
		//设置代表billboard postion的point，默认BBO_CENTER，但为了当雨滴触及水面时及时作出反应，修改到了底部

	}

public:
	bool frameStarted(const FrameEvent& evt)
	{
		if(MyBaseApplication::frameStarted(evt)==false)
			return false;

		//测试用
		//static bool pressedBefore=false;
		//if(_key->isKeyDown(OIS::KC_SPACE))
		//{
		//	if(!pressedBefore)
		//	{
		//		pressedBefore=true;
		//		mpWaterMesh->push((COMPLEXITY+1)/2,(COMPLEXITY+1)/2,-250);
		//		//mpWaterMesh->push((COMPLEXITY+1)/2,(COMPLEXITY+1)/2,-250,true);
		//	}
		//}
		//else
		//{
		//	pressedBefore=false;
		//}
		if(_key->isKeyDown(OIS::KC_SPACE))
		{
			particleEmitter->setEmissionRate(20.0f);
		}
		else
		{
			particleEmitter->setEmissionRate(0.0f);
		}
		processParticles();
			
		mpWaterMesh->updateMesh(evt.timeSinceLastFrame);


		return true;
	}



};