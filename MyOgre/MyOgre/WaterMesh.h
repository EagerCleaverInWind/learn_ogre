#pragma  once

#include "utility.h"

class WaterMesh
{
private:
	MeshPtr mesh ;
	SubMesh *subMesh ; 
	String meshName ;
	int complexity ;
	int numFaces ;
	int numVertices ;

	//”后台“计算用
	Vector3* posBuffers[3] ;
	int currentBuffNumber ;
	Vector3* vNormals ;

	Real lastFrameTime ;//just for push
	Real animationTime;
	Real timeFragment;//来自上一帧不足以完成一次状态池更换的残留时间

	HardwareVertexBufferSharedPtr posVertexBuffer ;
	HardwareVertexBufferSharedPtr normVertexBuffer ;
	HardwareVertexBufferSharedPtr texcoordsVertexBuffer ;
	HardwareIndexBufferSharedPtr indexBuffer ;

	Real C ; // ripple speed 
	Real D ; // distance
	Real U ; // viscosity
	Real T ; // time


private:
	void calculateNormals();


public:
	WaterMesh(const String& meshName, Real planeSize, int complexity) ;
    ~WaterMesh ();

	void push(Real x, Real y, Real depth, bool absolute=false) ;
	Real getHeight(Real x, Real y);
	void updateMesh(Real timeSinceLastFrame) ;
	
} ;