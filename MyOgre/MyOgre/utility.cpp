#include "utility.h"

void MyBaseApplication::LoadResources()
{
	Ogre::ConfigFile cf;
	cf.load("resources_d.cfg");

	Ogre::ConfigFile::SectionIterator sectionIter = cf.getSectionIterator();
	Ogre::String sectionName, typeName, dataname;
	while (sectionIter.hasMoreElements())
	{
		sectionName = sectionIter.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = sectionIter.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for (i = settings->begin(); i != settings->end(); ++i)
		{
			typeName = i->first;
			dataname = i->second;

			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
				dataname, typeName, sectionName);
		}
	}
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

void MyBaseApplication::InitInput()
{
	size_t windowHnd = 0;
	std::stringstream windowHndStr;
	_window->getCustomAttribute("WINDOW", &windowHnd);
	windowHndStr << windowHnd;
	OIS::ParamList pl;
	pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));
	_inputManager = OIS::InputManager::createInputSystem( pl );
	_key = static_cast<OIS::Keyboard*>(_inputManager->createInputObject( OIS::OISKeyboard, false ));
	_mouse = static_cast<OIS::Mouse*>(_inputManager->createInputObject( OIS::OISMouse, false ));
}

void MyBaseApplication::UpdateCamera(float dt)
{
	Ogre::Vector3 translate(0,0,0);

	if(_key->isKeyDown(OIS::KC_W))
	{
		translate += Ogre::Vector3(0,0,-1);
	}
	if(_key->isKeyDown(OIS::KC_S))
	{
		translate += Ogre::Vector3(0,0,1);
	}
	if(_key->isKeyDown(OIS::KC_A))
	{
		translate += Ogre::Vector3(-1,0,0);
	}
	if(_key->isKeyDown(OIS::KC_D))
	{
		translate += Ogre::Vector3(1,0,0);
	}
	translate.normalise();

	if(_key->isKeyDown(OIS::KC_R) && !_pressedBefore)
	{
		_pressedBefore = true;
		if(_polyMode == PM_SOLID) 
		{
			_polyMode = Ogre::PolygonMode::PM_WIREFRAME;
		} 
		else if(_polyMode == PM_WIREFRAME) 
		{
			_polyMode = Ogre::PolygonMode::PM_POINTS;
		} 
		else if(_polyMode == PM_POINTS) 
		{
			_polyMode = Ogre::PolygonMode::PM_SOLID;
		} 
		_cam->setPolygonMode(_polyMode);
	}
	if(!_key->isKeyDown(OIS::KC_R))
	{
		_pressedBefore = false;
	}

	float rotX = _mouse->getMouseState().X.rel * dt* -1;
	float rotY = _mouse->getMouseState().Y.rel * dt * -1;


	_cam->yaw(Ogre::Radian(rotX));
	_cam->pitch(Ogre::Radian(rotY));
	_cam->moveRelative(translate*dt * _movementspeed);
}


int MyBaseApplication::Startup()
{
	_root = new Ogre::Root("plugins_d.cfg");
	if(!_root->showConfigDialog())
	{
		return -1;
	}

	_window = _root->initialise(true,"MyOgre");

	_sceneManager = _root->createSceneManager(Ogre::ST_GENERIC);

	_cam = _sceneManager->createCamera("camera1");
	_cam->setPosition(Ogre::Vector3(0,100,200));
	_cam->lookAt(Ogre::Vector3(0,0,0));
	_cam->setNearClipDistance(5);

	_vp = _window->addViewport(_cam);
	_vp->setBackgroundColour(ColourValue(0.0f,0.0f,0.0f));
	_cam->setAspectRatio(Real(_vp->getActualWidth()) / Real(_vp->getActualHeight()));

	LoadResources();
	CreateScene();

	InitInput();
	_root->addFrameListener(this);

	return 0;

}

bool MyBaseApplication::frameStarted(const FrameEvent& evt)
{
	_key->capture();
	_mouse->capture();

	if(_key->isKeyDown(OIS::KC_ESCAPE))
	{
		return false;
	}

	float dt=evt.timeSinceLastFrame;
	UpdateCamera(dt);

	return true;
}

void MyBaseApplication::RenderOneFrame()
{
	Ogre::WindowEventUtilities::messagePump();
	_keepRunning = _root->renderOneFrame();
}
