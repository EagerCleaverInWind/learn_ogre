#pragma  once

#include "utility.h"

class CubeMappingDemo:public MyBaseApplication,public RenderTargetListener
{
private:
	Entity* mMappedObject;
	Camera* mCubeCamera;
	RenderTarget* mTargets[6];
	SceneNode* mPivot;
	AnimationState* mFishSwim;

private:
	void CreateCubeMap();

protected:
	void CreateScene();


public:
	CubeMappingDemo()
	{
		mMappedObject=nullptr;
		mCubeCamera=nullptr;
		mPivot=nullptr;
		mFishSwim=nullptr;
		for(int i=0;i<6;i++)
			mTargets[i]=nullptr;
	}
	~CubeMappingDemo()
	{
		_sceneManager->destroyCamera(mCubeCamera);
		MeshManager::getSingleton().remove("floor");
		TextureManager::getSingleton().remove("dyncubemap");
	}

	void preRenderTargetUpdate(const RenderTargetEvent& evt);
	void postRenderTargetUpdate(const RenderTargetEvent& evt);
	bool frameStarted(const FrameEvent& evt);
};