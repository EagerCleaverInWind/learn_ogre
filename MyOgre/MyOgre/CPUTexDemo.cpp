#include "CPUTexDemo.h"

void CPUTexDemo::updateTexture(uint8 freezeAmount)
{
	float* data = mConstantTexBuf;
	UINT8* integerate=mIntermediateTexBuf;

	float temperature;
	float sqrRange=Math::Sqr(TEXTURE_SIZE/2);
	float sqrDistToCenter;
	float add;//vary from sqrDistToCenter

	for (unsigned int y = 0; y < TEXTURE_SIZE; y++)
	{
		for (unsigned int x = 0; x < TEXTURE_SIZE; x++)
		{
			if (freezeAmount != 0)
			{
				//sqrDistToCenter = Math::Sqr(x - TEXTURE_SIZE/2) + Math::Sqr(y - TEXTURE_SIZE/2);
				//垃圾Ogre的Math::Sqr()！！！！负数时不行！！！害得只有右下角1/4区域有效，就算尺寸再小像素再低根据sample流程也不可能得出这种结果
				sqrDistToCenter = (x - TEXTURE_SIZE/2)*(x - TEXTURE_SIZE/2) + (y - TEXTURE_SIZE/2)*(y - TEXTURE_SIZE/2);
				add=std::max<float>(sqrDistToCenter/sqrRange,1/10.0f)*freezeAmount;
				*data += add;
				if(*data>255.0f)
					*data =  255.0f;
			}
			*integerate=(int)(*data);
			integerate++;
			data++;
		}
	}

	memcpy(mTexBuf->lock(HardwareBuffer::HBL_DISCARD), mIntermediateTexBuf, TEXTURE_SIZE * TEXTURE_SIZE);
	mTexBuf->unlock();
}

void CPUTexDemo::UpdateTexture(uint8 freezeAmount)
{
	// get access to raw texel data
	uint8* data = mIntermediateTexBuf;
	uint8 temperature;

	// go through every texel...
	for (unsigned int y = 0; y < TEXTURE_SIZE; y++)
	{
		for (unsigned int x = 0; x < TEXTURE_SIZE; x++)
		{
			if (freezeAmount != 0)
			{
				// gradually refreeze anything that isn't completely frozen
				temperature = 0xff - *data;
				if (temperature > freezeAmount) *data += freezeAmount;
				else *data = 0xff;
			}
			data++;
		}
	}

	if(_key->isKeyDown(OIS::KC_SPACE))
		//memset(mIntermediateTexBuf,0,sizeof(mIntermediateTexBuf));//请注意！（倒车！）这是个指针而不是数组名！所以我知道用mConstantTexBuf哪里错了！！！
		memset(mIntermediateTexBuf,0,mTexBuf->getSizeInBytes());
	
	memcpy(mTexBuf->lock(HardwareBuffer::HBL_DISCARD), mIntermediateTexBuf, TEXTURE_SIZE * TEXTURE_SIZE);
	mTexBuf->unlock();
}

void CPUTexDemo::CreateScene()
{
	_sceneManager->setSkyBox(true, "Examples/StormySkyBox");  // add a skybox

	// setup some basic lighting for our scene
	_sceneManager->setAmbientLight(ColourValue(0.5, 0.5, 0.5));
	_sceneManager->createLight()->setPosition(20, 80, 50);

	_cam->setPosition(0,0,200);
	_cam->lookAt(0,0,0);

	// create our dynamic texture with 8-bit luminance texels
	TexturePtr tex = TextureManager::getSingleton().createManual("thaw", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
		TEX_TYPE_2D, TEXTURE_SIZE, TEXTURE_SIZE, 0, PF_L8, TU_DYNAMIC_WRITE_ONLY);

	mTexBuf = tex->getBuffer();  // save off the texture buffer
	
	// initialise the texture to have zero luminance i.e. absolutely transparent
	mConstantTexBuf=new float[mTexBuf->getSizeInBytes()];
	//memset(mConstantTexBuf,0,sizeof(mConstantTexBuf));
	memset(mConstantTexBuf,0,sizeof(float)*mTexBuf->getSizeInBytes());

	mIntermediateTexBuf = new uint8[mTexBuf->getSizeInBytes()];
	memset(mIntermediateTexBuf, 0, mTexBuf->getSizeInBytes());
	memcpy(mTexBuf->lock(HardwareBuffer::HBL_DISCARD), mIntermediateTexBuf, TEXTURE_SIZE * TEXTURE_SIZE);
	mTexBuf->unlock();

	// create a penguin and attach him to our penguin node
	Entity* penguin = _sceneManager->createEntity("Penguin", "penguin.mesh");
	mPenguinNode = _sceneManager->getRootSceneNode()->createChildSceneNode();
	mPenguinNode->attachObject(penguin);

	// get and enable the penguin idle animation
	mPenguinAnimState = penguin->getAnimationState("amuse");
	mPenguinAnimState->setEnabled(true);

	// create a snowstorm over the scene, and fast forward it a little
	ParticleSystem* ps = _sceneManager->createParticleSystem("Snow", "Examples/Snow");
	_sceneManager->getRootSceneNode()->attachObject(ps);
	ps->fastForward(30);

	// create a frosted screen in front of the camera, using our dynamic texture to "thaw" certain areas
	Entity* ent = _sceneManager->createEntity("Plane", SceneManager::PT_PLANE);
	ent->setMaterialName("MtrFrost");
	SceneNode* node = _sceneManager->getRootSceneNode()->createChildSceneNode();
	node->setPosition(0, 0, 50);
	node->attachObject(ent);

	mTimeSinceLastFreeze = 0;
}

bool CPUTexDemo::frameStarted(const FrameEvent& evt)
{
	_key->capture();
	if(_key->isKeyDown(OIS::KC_ESCAPE))
	{
		return false;
	}

	uint8 freezeAmount = 0;
	mTimeSinceLastFreeze += evt.timeSinceLastFrame;
	// find out how much to freeze the plane based on time passed
	while (mTimeSinceLastFreeze >= 0.1)
	{
		mTimeSinceLastFreeze -= 0.1;
		freezeAmount += 0x04;
	}
	//updateTexture(freezeAmount);  // rebuild texture contents
	updateTexture(freezeAmount);

	mPenguinAnimState->addTime(evt.timeSinceLastFrame);  // increment penguin idle animation time
	mPenguinNode->yaw(Radian(evt.timeSinceLastFrame));   // spin the penguin around

	return MyBaseApplication::frameStarted(evt);
}