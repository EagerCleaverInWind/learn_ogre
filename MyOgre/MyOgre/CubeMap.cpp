#include "CubeMap.h"

void CubeMappingDemo::CreateCubeMap()
{
	// create the camera used to render to our cubemap
	mCubeCamera = _sceneManager->createCamera("CubeMapCamera");
	mCubeCamera->setFOVy(Degree(90));
	mCubeCamera->setAspectRatio(1);
	mCubeCamera->setFixedYawAxis(false);
	mCubeCamera->setNearClipDistance(5);

	// create our dynamic cube map texture
	TexturePtr tex = TextureManager::getSingleton().createManual("dyncubemap",
		ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, TEX_TYPE_CUBE_MAP, 128, 128, 0, PF_R8G8B8, TU_RENDERTARGET);

	// assign our camera to all 6 render targets of the texture (1 for each direction)
	for (unsigned int i = 0; i < 6; i++)
	{
		mTargets[i] = tex->getBuffer(i)->getRenderTarget();
		mTargets[i]->addViewport(mCubeCamera)->setOverlaysEnabled(false);
		mTargets[i]->addListener(this);
	}
}

void CubeMappingDemo::CreateScene()
{
	_sceneManager->setSkyDome(true, "Examples/CloudySky");

	// setup some basic lighting for our scene
	_sceneManager->setAmbientLight(ColourValue(0.3, 0.3, 0.3));
	_sceneManager->createLight()->setPosition(20, 80, 50);

	CreateCubeMap();

	// create an ogre head, give it the dynamic cube map material, and place it at the origin
	mMappedObject = _sceneManager->createEntity("CubeMappedHead", "ogrehead.mesh");
	mMappedObject->setMaterialName("mtrDynamicCubeMap");
	_sceneManager->getRootSceneNode()->attachObject(mMappedObject);

	mPivot = _sceneManager->getRootSceneNode()->createChildSceneNode();  // create a pivot node
	Entity* fish = _sceneManager->createEntity("Fish", "fish.mesh");
	mFishSwim = fish->getAnimationState("swim");
	mFishSwim->setEnabled(true);
	// create a child node at an offset and attach a regular ogre head and a nimbus to it
	SceneNode* node = mPivot->createChildSceneNode(Vector3(-60, 10, 0));
	node->setScale(7, 7, 7);
	node->yaw(Degree(90));
	node->attachObject(fish);

	// create a floor mesh resource
	MeshManager::getSingleton().createPlane("floor", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
		Plane(Vector3::UNIT_Y, -30), 1000, 1000, 10, 10, true, 1, 8, 8, Vector3::UNIT_Z);
	// create a floor entity, give it a material, and place it at the origin
	Entity* floor = _sceneManager->createEntity("Floor", "floor");
	floor->setMaterialName("Examples/BumpyMetal");
	_sceneManager->getRootSceneNode()->attachObject(floor);
}

void CubeMappingDemo::preRenderTargetUpdate(const RenderTargetEvent& evt)
{
	mMappedObject->setVisible(false);  // hide the head

	// point the camera in the right direction based on which face of the cubemap this is
	mCubeCamera->setOrientation(Quaternion::IDENTITY);
	if (evt.source == mTargets[0]) mCubeCamera->yaw(Degree(-90));
	else if (evt.source == mTargets[1]) mCubeCamera->yaw(Degree(90));
	else if (evt.source == mTargets[2]) mCubeCamera->pitch(Degree(90));
	else if (evt.source == mTargets[3]) mCubeCamera->pitch(Degree(-90));
	else if (evt.source == mTargets[5]) mCubeCamera->yaw(Degree(180));
}
void CubeMappingDemo::postRenderTargetUpdate(const RenderTargetEvent& evt)
{
	mMappedObject->setVisible(true);// unhide the head
}
bool CubeMappingDemo::frameStarted(const FrameEvent& evt)
{
	mPivot->yaw(Radian(evt.timeSinceLastFrame));      // spin the fishy around the cube mapped one
	mFishSwim->addTime(evt.timeSinceLastFrame * 3);   // make the fishy swim
	return MyBaseApplication::frameStarted(evt);

}