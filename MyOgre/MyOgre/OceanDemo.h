#include "utility.h"

class OceanDemo:public MyBaseApplication
{
private:
	GpuProgramParametersSharedPtr mVPPPtr;//修改shader参数（hlsl中的全局变量）用
	float mWaveAmp;


protected:
	void CreateScene()
	{
		_sceneManager->setAmbientLight(Ogre::ColourValue(0.3, 0.3, 0.3));
		_sceneManager->setSkyBox(true, "SkyBox", 1000);

		Ogre::Plane oceanSurface;
		oceanSurface.normal = Ogre::Vector3::UNIT_Y;
		oceanSurface.d = 20;
		Ogre::MeshManager::getSingleton().createPlane("OceanSurface",
			Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
			oceanSurface,
			1000, 1000, 50, 50, true, 1, 1, 1, Ogre::Vector3::UNIT_Z);

		Entity* oceanSurfaceEnt = _sceneManager->createEntity( "MyOcean", "OceanSurface" );
		oceanSurfaceEnt->setMaterialName("MtrMyOcean");
		_sceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(oceanSurfaceEnt);

		MaterialPtr mtrPtr=Ogre::MaterialManager::getSingleton().getByName("MtrMyOcean");
		mVPPPtr=mtrPtr->getTechnique(0)->getPass(0)->getVertexProgramParameters();
		mWaveAmp=5.0f;
	}


public:
	bool frameStarted(const FrameEvent& evt)
	{
		if(_key->isKeyDown(OIS::KC_U))
		{
			if(mWaveAmp<14.99f)
			mWaveAmp+=0.01f;
			mVPPPtr->setNamedConstant("waveAmp",mWaveAmp);
		}

		if(_key->isKeyDown(OIS::KC_J))
		{
			if(mWaveAmp>0.01f)
				mWaveAmp-=0.01f;
			mVPPPtr->setNamedConstant("waveAmp",mWaveAmp);
		}

		return MyBaseApplication::frameStarted(evt);
	}
	
};