#pragma  once
#include "utility.h"
#include<vector>
class ParticleSystemDemo:public MyBaseApplication
{
	struct PSToggle
	{
		ParticleSystem* _ps;
		bool _on;
		bool _pressedBefore;
	};
private:
	AnimationState* mFountainAnimState;
	AnimationState* mPointLightAnimState;
	std::vector<PSToggle> mPSVector;
	SceneNode* mSinbadNode;
	SceneNode* mFountainNode;

private:
	void SetupPointLight()
	{
		_sceneManager->setAmbientLight(Ogre::ColourValue(0.15f, 0.15f, 0.15f));


		// Create a ribbon trail that our lights will leave behind
		RibbonTrail* lightTrail;
		NameValuePairList params;
		params["numberOfChains"] = "1";
		params["maxElements"] = "80";
		lightTrail = (RibbonTrail*)_sceneManager->createMovableObject("RibbonTrail", &params);
		_sceneManager->getRootSceneNode()->attachObject(lightTrail);//just for visible
		lightTrail->setMaterialName("Examples/LightRibbonTrail");
		lightTrail->setTrailLength(400);

		SceneNode* node;
		Animation* anim;
		NodeAnimationTrack* track;
		Light* light;
		BillboardSet* bbs;//一组相同的billboard

		// Create a light node
		node = _sceneManager->getRootSceneNode()->createChildSceneNode(Vector3(50, 30, 0));

		// Create a 14 second animation with spline interpolation
		anim = _sceneManager->createAnimation("Path1", 14);
		anim->setInterpolationMode(Animation::IM_SPLINE);
		track = anim->createNodeTrack(0, node);  // Create a node track for our animation
		// Enter keyframes for our track to define a path for the light to follow
		track->createNodeKeyFrame(0)->setTranslate(Vector3(50, 130, 0));
		track->createNodeKeyFrame(2)->setTranslate(Vector3(100, 70, 0));
		track->createNodeKeyFrame(4)->setTranslate(Vector3(120,20, 150));
		track->createNodeKeyFrame(6)->setTranslate(Vector3(30, 20, 50));
		track->createNodeKeyFrame(8)->setTranslate(Vector3(-50, 130, -50));
		track->createNodeKeyFrame(10)->setTranslate(Vector3(-150, 80, -100));
		track->createNodeKeyFrame(12)->setTranslate(Vector3(-50, 70, 0));
		track->createNodeKeyFrame(14)->setTranslate(Vector3(50, 130, 0));
		// Create an animation state from the animation and enable it
		mPointLightAnimState = _sceneManager->createAnimationState("Path1");
		mPointLightAnimState->setEnabled(true);

		// Set initial settings for the ribbon lightTrail and add the light node
		lightTrail->setInitialColour(0,1.0, 1.0, 1.0, 1.0);
		lightTrail->setColourChange(0, 0.5, 0.5, 0.5, 0.5);
		lightTrail->setInitialWidth(0, 5);
		lightTrail->addNode(node);


		// Attach a light with the same colour to the light node
		light = _sceneManager->createLight();
		light->setDiffuseColour(lightTrail->getInitialColour(0));
		node->attachObject(light);

		// Attach a flare with the same colour to the light node
		bbs = _sceneManager->createBillboardSet(1);
		bbs->createBillboard(Vector3::ZERO, lightTrail->getInitialColour(0));//添加一个billboard
		bbs->setMaterialName("Examples/Flare");
		bbs->setDefaultDimensions(30,30);
		node->attachObject(bbs);

	}



public:
	ParticleSystemDemo():mFountainAnimState(nullptr),mSinbadNode(nullptr),mFountainNode(nullptr),mPointLightAnimState(nullptr)
	{
		mPSVector.resize(5);
		for(UINT i=0;i<5;i++)
		{
			mPSVector[i]._on=false;
			mPSVector[i]._pressedBefore=false;
		}
			
	}

	void CreateScene()
	{
		SetupPointLight();

		Ogre::Plane plane(Vector3::UNIT_Y, -10);
		Ogre::MeshManager::getSingleton().createPlane("plane",
			ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane,
			1500,1500,200,200,true,1,25,25,Vector3::UNIT_Z);
		Ogre::Entity* ent = _sceneManager->createEntity("LightPlaneEntity", "plane");
		_sceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(ent);
		ent->setMaterialName("Examples/BeachStones");

		Entity* sinbadEnt= _sceneManager->createEntity("Sinbad", "Sinbad.mesh");
		mSinbadNode=_sceneManager->getRootSceneNode()->createChildSceneNode("SinbadNode");
		mSinbadNode->setScale(3.0f,3.0f,3.0f);
		mSinbadNode->setPosition(Ogre::Vector3(0.0f,4.0f,0.0f));
		mSinbadNode->attachObject(sinbadEnt);

		 mFountainNode=_sceneManager->getRootSceneNode()->createChildSceneNode();
		 //mFountainNode->setScale(20,20,20);//试一下会不会影响粒子,结果发现不会
		Animation* anim =_sceneManager->createAnimation("FountainTrack",10);
		NodeAnimationTrack* track = anim->createNodeTrack(0, mFountainNode);
		// create keyframes for our track
		track->createNodeKeyFrame(0)->setTranslate(Vector3(-200, 0, -100));
		track->createNodeKeyFrame(1.5)->setTranslate(Vector3(0, 0, 0));
		track->createNodeKeyFrame(3)->setTranslate(Vector3(200, 0, 100));
		track->createNodeKeyFrame(4)->setTranslate(Vector3(300, 0, 0));
		track->createNodeKeyFrame(5)->setTranslate(Vector3(200, 0, -100));
		track->createNodeKeyFrame(6.5)->setTranslate(Vector3(0, 0, 0));
		track->createNodeKeyFrame(8)->setTranslate(Vector3(-200, 0, 100));
		track->createNodeKeyFrame(9)->setTranslate(Vector3(-300, 0, 0));
		track->createNodeKeyFrame(10)->setTranslate(Vector3(-200, 0,-100));
		// create a new animation state to track this
		mFountainAnimState = _sceneManager->createAnimationState("FountainTrack");
		mFountainAnimState->setEnabled(true);

		mPSVector[4]._ps = _sceneManager->createParticleSystem("fountain","MyFountain");
		mPSVector[3]._ps = _sceneManager->createParticleSystem("hasaki","MyCyclone");
		mPSVector[2]._ps = _sceneManager->createParticleSystem("rain","MyRain");
		mPSVector[1]._ps = _sceneManager->createParticleSystem("firework","MyFirework");
		mPSVector[0]._ps = _sceneManager->createParticleSystem("nimbus","MyNimbus");
		for(int i=0;i<4;i++)
		{
			mSinbadNode->attachObject(mPSVector[i]._ps);
			mPSVector[i]._ps->setVisible(false);
		}
		mFountainNode->attachObject(mPSVector[4]._ps);
		mPSVector[4]._ps->setVisible(false);
	}

	bool frameStarted(const FrameEvent& evt)
	{
		mFountainAnimState->addTime(evt.timeSinceLastFrame);
		mPointLightAnimState->addTime(evt.timeSinceLastFrame);

		for(UINT i=0;i<4;i++)
		{
			if(_key->isKeyDown(OIS::KeyCode(i+2)) )
			{
				if(!mPSVector[i]._pressedBefore)
				{
					mPSVector[i]._pressedBefore=true;
					if(mPSVector[i]._on)
					{
						//mSinbadNode->detachObject(mPSVector[i]._ps);
						mPSVector[i]._ps->setVisible(false);
						mPSVector[i]._on=false;
					}	
					else
					{
						//mSinbadNode->attachObject(mPSVector[i]._ps);
						mPSVector[i]._ps->setVisible(true);
						mPSVector[i]._on=true;
					}
						
				}
			}
			else
			{
				mPSVector[i]._pressedBefore=false;
			}

		}


		if(_key->isKeyDown(OIS::KC_5) )
		{
			if(!mPSVector[4]._pressedBefore)
			{
				mPSVector[4]._pressedBefore=true;
				if(mPSVector[4]._on)
				{
					//mFountainNode->detachObject(mPSVector[4]._ps);
					mPSVector[4]._ps->setVisible(false);
					mPSVector[4]._on=false;
				}	
				else
				{
					//mFountainNode->attachObject(mPSVector[4]._ps);
					mPSVector[4]._ps->setVisible(true);
					mPSVector[4]._on=true;
				}
			}
		}
		else
		{
			mPSVector[4]._pressedBefore=false;
		}

		return MyBaseApplication::frameStarted(evt);
	}

};